# Example:

```rust
let mut args = ArgParser::new("rargsxd_example");
args.info("A test example for rargsxd")
    .author("BubbyRoosh")
    .version("0.1.0")
    .copyright("Copyright (C) 2021 BubbyRoosh")
    // All instances of {name} are replaced with the name of the program.
    .usage("{name} [arguments] [other ...]")
    .args(vec!(
    	Arg::bool("test", false)
    	    .short('t')
    	    .help("Test boolean argument"),
    	Arg::str("long", "")
    	    .long("long-argument")
    	    .help("Long argument test"),
    )).parse();

println!("{}", args.get_bool("test"));
println!("{}", args.get_str("long"));
println!("{:?}", args.extra);
```

Assuming the args passed are: -t --long-argument word another word

It would output:
```
word
true
["another", "word"]
```
