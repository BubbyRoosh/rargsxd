#[derive(Debug, Clone)]
/// Value of an argument
pub enum ArgVal {
    Bool(bool),
    Str(String),
}

#[derive(Debug, Clone)]
/// Argument struct
pub struct Arg {
    /// Name of the argument
    pub (crate) name: String,
    /// Optional long (string) value of the argument
    pub (crate) long: Option<String>,
    /// Optional short (char) value of the argument
    pub (crate) short: Option<char>,
    /// Help string for the argument
    pub (crate) help: String,
    /// Value of the argument
    pub (crate) val: ArgVal,
}

impl Arg {
    /// Creates a new bool (flag) argument
    pub fn bool(name: &str, val: bool) -> Self {
        Self {
            name: String::from(name),
            long: None,
            short: None,
            help: String::new(),
            val: ArgVal::Bool(val),
        }
    }

    /// Creates a new str (option) argument
    pub fn str(name: &str, val: &str) -> Self {
        Self {
            name: String::from(name),
            long: None,
            short: None,
            help: String::new(),
            val: ArgVal::Str(String::from(val)),
        }
    }

    /// Sets the long value of the argument
    pub fn long(&mut self, long: &str) -> &mut Self {
        self.long = Some(String::from(long));
        self
    }

    /// Sets the short value of the argument
    pub fn short(&mut self, short: char) -> &mut Self {
        self.short = Some(short);
        self
    }

    /// Sets the help string of the argument
    pub fn help(&mut self, help: &str) -> &mut Self {
        self.help = String::from(help);
        self
    }
}
